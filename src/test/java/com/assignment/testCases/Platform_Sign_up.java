package com.assignment.testCases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.assignment.commonFiles.CommonFunctionalities;
import com.assignment.pom.HomePage;
import com.assignment.pom.Registration;
import com.assignment.utils.getData;

public class Platform_Sign_up extends CommonFunctionalities {

  @Test
  public void testCase() throws InterruptedException, IOException {
	  HomePage hp = new HomePage(driver);
	  Registration reg = new Registration(driver);
	  System.out.println("---------------Sign Up Starts------------------");
	  hp.signUp().click();
	  wait = new WebDriverWait(driver,30);
	  wait.until(ExpectedConditions.visibilityOfElementLocated((By.id("IDS_UI_Window"))));
	  driver.switchTo().frame("IDS_UI_Window");
	  reg.fName().sendKeys(fName);
	  reg.lName().sendKeys(lName);
	  reg.email().sendKeys(email);
	  reg.password().sendKeys(password);
	  reg.reEnterpassword().sendKeys(password);
	  checkBoxSelected(reg.privacy());
	  checkBoxSelected(reg.terms());
	  reg.registerButton().click();
	  System.out.println("---------------Sign Up Ends------------------");

  }
}
