package com.assignment.commonFiles;


import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.assignment.utils.getData;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;

public class CommonFunctionalities{
	public static WebDriver driver;
	public static String mainWindow;
	public static String childWindow;
	public static Set<String> windows;
	public static Iterator<String> itr;
	public static String fName;
	public static String lName;
	public static String email;
	public static String password;
	public static WebDriverWait wait; 
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String testName;
	
  @BeforeSuite
  @Parameters("browser")
  public void browserNeed(String bro) throws IOException {
	  //Zalenium
	  DesiredCapabilities dc = new DesiredCapabilities();
	  if (bro.equalsIgnoreCase("Chrome"))
	  {
		  dc.setCapability(CapabilityType.BROWSER_NAME, BrowserType.CHROME);
		  dc.setCapability(CapabilityType.PLATFORM_NAME, Platform.LINUX);
	  }
	  URL url = new URL(getData.retunPropValue("SeleniumGrid"));
	  driver = new RemoteWebDriver(url,dc);
	  //Local Chrome
//	  System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//resources//drivers//chromedriver.exe");
//	  driver = new ChromeDriver();
	  driver.get(getData.retunPropValue("URL"));
	  driver.manage().window().maximize();
  }

  @AfterSuite
  public void closeBrowser() {
	  driver.quit();
  }
  
  public void switchToChild()
  {
	  System.out.println("Child window process");
	  mainWindow=driver.getWindowHandle();
	  
	  windows = driver.getWindowHandles();
	  itr = windows.iterator();
	  
	  childWindow = itr.next();
	  driver.switchTo().window(childWindow);
	  
	  System.out.println(childWindow);
  }
  
  public void switchToMainWindow()
  {
	  driver.switchTo().window(mainWindow);
  }
  
  public void checkBoxSelected(WebElement checkBox)
  {
	  WebElement viewElement = checkBox;
	  JavascriptExecutor jse2 = (JavascriptExecutor)driver;
	  jse2.executeScript("arguments[0].scrollIntoView()", viewElement); 
	  
	  if(checkBox.isSelected())
	  {
		  System.out.println("Checkbox Already selected");
	  }
	  checkBox.click();
	  
  }
  
  @BeforeTest
  public void userDetails() throws IOException
  {
	  int sheeNumber = 0;
	  int rowNum = 1;
	  getData.getExcelValue(sheeNumber,rowNum);
	  fName = getData.excelValues.get("FirstName");
	  lName = getData.excelValues.get("Lastname");
	  email = getData.excelValues.get("Email");
	  password = getData.excelValues.get("Password");
  }
  
  @BeforeTest
  public void initializeReport()
  {
	  ExtentReportGenerator.reportGeneration();
	  test = extent.createTest("Assignment for ITC");
  }
  
  
  @AfterTest
  public void exitReport()
  {
	  extent.flush();
  }


}
