package com.assignment.commonFiles;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentReportGenerator extends CommonFunctionalities {

	public static ExtentReports reportGeneration()
	{
		ExtentSparkReporter report = new ExtentSparkReporter(System.getProperty("user.dir")+"\\target\\reports\\extentReport.html");
		report.config().setReportName("results");
		report.config().setDocumentTitle("Test Results");
		extent = new ExtentReports();
		extent.attachReporter(report);
		extent.setSystemInfo("Tester", "Veena");
		return extent;
	}
}
