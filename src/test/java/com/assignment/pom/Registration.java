package com.assignment.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Registration {
	
	WebDriver driver;
	@FindBy(xpath = "//input[@id='firstName']")
	WebElement fName;
	
	@FindBy(xpath = "//input[@id='lastName']")
	WebElement lName;
	
	@FindBy(xpath = "//input[@id='mail']")
	WebElement email;
	
	@FindBy(xpath = "//input[@id='newPasswordInput']")
	WebElement password;
	
	@FindBy(xpath = "//input[@id='retypeNewPasswordInput']")
	WebElement reEnterpassword;
	
	@FindBy(xpath = "//*[@id='pdAccept']")
	WebElement privacy;
	
	@FindBy(xpath = "//*[@id='touAccept']")
	WebElement terms;
	
	@FindBy(xpath = "//button[@id='sapStoreRegisterFormSubmit']")
	WebElement registerButton;
	
	public Registration(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement fName()
	{
		return fName;
	}

	public WebElement lName()
	{
		return lName;
	}
	
	public WebElement email()
	{
		return email;
	}
	
	public WebElement password()
	{
		return password;
	}
	
	public WebElement reEnterpassword()
	{
		return reEnterpassword;
	}
	
	public WebElement privacy()
	{
		return privacy;
	}
	
	public WebElement terms()
	{
		return terms;
	}
	
	public WebElement registerButton()
	{
		return registerButton;
	}
}
