package com.assignment.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Properties;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class getData{
	
	public static XSSFWorkbook wb;
	public static XSSFSheet sh;
	public static XSSFRow row;
	public static File file;
	public static FileInputStream fis;
	public static Properties p;
	public static HashMap<String,String> excelValues;
	
	public static void getExcelValue(int sheeNumber,int rowNum) throws IOException
	{
		file = new File(System.getProperty("user.dir")+"\\resources\\test-data.xlsx");
		fis = new FileInputStream(file);
		wb = new XSSFWorkbook(fis);
		sh = wb.getSheetAt(sheeNumber);
		row = sh.getRow(0);
		excelValues = new HashMap<String,String>();
		
		//Data formatter to convert all the excel value to String
		DataFormatter objDefaultFormat = new DataFormatter();
		
		for(int k=0; k<=row.getLastCellNum(); k++)
		{
			XSSFCell cell1 = sh.getRow(0).getCell(k); 
			String key = objDefaultFormat.formatCellValue(cell1);
			XSSFCell cell2 = sh.getRow(rowNum).getCell(k);
			String value = objDefaultFormat.formatCellValue(cell2);
			excelValues.put(key,value);
		}
	}
	
	public static String retunPropValue(String value) throws IOException
	{
		fis = new FileInputStream(System.getProperty("user.dir")+"\\resources\\config.properties");
		p = new Properties();
		p.load(fis);
		return p.getProperty(value);
	}
}
